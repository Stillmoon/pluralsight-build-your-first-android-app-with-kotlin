package com.example.notekeeper

object DataManager {
    val courses = HashMap<String, CourseInfo>()
    val notes = ArrayList<NoteInfo>()

    init {
        initializeCourse()
        initializeNotes()
    }

    private fun initializeCourse() {
        var course = CourseInfo("android_intents", "Android Programming with Intents")
        courses.set(course.courseID, course)

        course = CourseInfo(courseID = "android_async", title = "Android Async Programming and Services")
        courses.set(course.courseID, course)

        course = CourseInfo(title = "Java Fundamentals: The Java Language", courseID = "java_lang")
        courses.set(course.courseID, course)

        course = CourseInfo(courseID = "java_core", title = "Java Fundamentals: The Core Platform")
        courses.set(course.courseID, course)
    }

    private fun initializeNotes() {
        var note = NoteInfo(courses["android_intents"]!!, "Awful Course", "They didn't even fucking tell me what his fucking notes were.")
        notes.add(note)
    }
}